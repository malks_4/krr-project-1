package com.company;
import java.util.ArrayList;

public class Clause {
    ArrayList<Literal> literals = new ArrayList<Literal>();

    void setClause(String clause){
        clause = clause.replace("[", "");
        clause = clause.replace("]","");
        clause = clause.replace(" ",""); //so that if user inputs clause with spaces, these are ignored
        String[] result = clause.split(",");

        for (String s : result) {
            Literal l = new Literal();
            l.polarity = !s.contains("!");
            s = s.replace("!","");
            l.name = s;
            literals.add(l);
        }
    }
}
