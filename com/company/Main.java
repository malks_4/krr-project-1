package com.company;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.lang.String;

public class Main {
    public static void main(String[] args) throws FileNotFoundException {
        File kbFile = new File(args[0]);
        Scanner scnr = new Scanner(kbFile);
        Scanner keyboard = new Scanner(System.in);
        KnowledgeBase kb = new KnowledgeBase();
        String clause;

        while(scnr.hasNextLine()){          //Loading KB from text file into objects
           clause = scnr.nextLine();
           Clause c = new Clause();
           c.setClause(clause);
           kb.clauses.add(c);
        }
        kb.getKB();
        Clause notAlpha = new Clause();
        System.out.println("Please enter the negative of your query:");
        clause = keyboard.nextLine();
        notAlpha.setClause(clause);
        boolean ans = kb.BChainingSolve(notAlpha);
        if(ans){
            System.out.println("SOLVED");
        }
        else{
            System.out.println("NOT SOLVED");
        }
    }
}
