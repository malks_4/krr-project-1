package com.company;
import java.util.ArrayList;

public class KnowledgeBase {
    ArrayList<Clause> clauses = new ArrayList<Clause>();

    void getKB(){   //this method was created to test parsing of strings and storage of data
        for (Clause clause : clauses) {
            System.out.print("[");
            for (int j = 0; j < clause.literals.size(); j++) {
                if (!clause.literals.get(j).polarity) {
                    System.out.print("!");
                }
                System.out.print(clause.literals.get(j).name);
                if (j < clause.literals.size() - 1) {
                    System.out.print(",");
                }
            }
            System.out.println("]");
        }
    }

    boolean BChainingSolve(Clause notAlpha){
        if(notAlpha.literals.size() == 0){
            return true;
        }
        else{
            for(int i = 0;i<notAlpha.literals.size();i++){
                for(int j = 0;j<clauses.size();j++){
                    for(int k = 0;k<clauses.get(j).literals.size();k++){
                        if((clauses.get(j).literals.get(k).name.equals(notAlpha.literals.get(i).name))&&(clauses.get(j).literals.get(k).polarity!=notAlpha.literals.get(i).polarity)){
                            Clause nClause = new Clause();
                            nClause = clauses.get(j);
                            nClause.literals.remove(k);
                            boolean ret = BChainingSolve(nClause);
                            if(ret){
                                return true;
                            }
                        }
                    }
                }
            }
        }
        return false;
    }
}


